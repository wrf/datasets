# WRF's datasets #

This is a repo only for hosting various large datasets, mostly for other repos

### Current files ###
for summaries of [NCBI SRA](https://www.ncbi.nlm.nih.gov/sra/) database metadata in [my taxonomy database repo](https://github.com/wrf/taxonomy_database)

* [NCBI_SRA_Metadata_Full_20220117.metagenomes_latlon-fixed.tab.gz](https://bitbucket.org/wrf/datasets/downloads/NCBI_SRA_Metadata_Full_20220117.metagenomes_latlon-fixed.tab.gz) table of 1654918row x 16col
* [NCBI_SRA_Metadata_Full_20220117.sample_w_exp.tab.gz](https://bitbucket.org/wrf/datasets/downloads/NCBI_SRA_Metadata_Full_20220117.sample_w_exp.tab.gz) table of 15934738row x 12col

* [NCBI_SRA_Metadata_Full_20210404.metagenomes_latlon-fixed.tab.gz](https://bitbucket.org/wrf/datasets/downloads/NCBI_SRA_Metadata_Full_20210404.metagenomes_latlon-fixed.tab.gz) table of 1301794row x 16col
* [NCBI_SRA_Metadata_Full_20210404.samples_experiment.tab.gz](https://bitbucket.org/wrf/datasets/downloads/NCBI_SRA_Metadata_Full_20210404.samples_experiment.tab.gz) table of 10838654row x 12col

for summaries of [NOAA WOD](https://www.ncei.noaa.gov/access/world-ocean-database-select/dbsearch.html) database in my [oceanography repo](https://github.com/wrf/oceanography_scripts)

* [ocldb1616358314.25649.OSD_all.all_vars.tab.gz](https://bitbucket.org/wrf/datasets/downloads/ocldb1616358314.25649.OSD_all.all_vars.tab.gz) table of 28987253row x 37col



